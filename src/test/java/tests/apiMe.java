package tests;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

import static io.restassured.RestAssured.given;
import static io.restassured.RestAssured.post;
import static org.hamcrest.Matchers.equalTo;

@Tag("API_ME")
public class apiMe {

    @BeforeAll
    public static void setBaseUrl() {
        RestAssured.baseURI = "https://api.tsxass-stage.talenttechlab.org";
    }

    @Test
    public void checkMyName() {

        String requestBody = "{\"phone\": \"05553621116\"}";
        Response response = RestAssured.given()
                .contentType(ContentType.JSON)
                .body(requestBody)
                .post("https://api.profile-stage.talenttechlab.org/auth/phone/request");

        String requestBody2 = "{\"phone\": \"05553621116\", \"code\": \"4628\"}";
        Response response2 = RestAssured.given()
                .contentType(ContentType.JSON)
                .body(requestBody2)
                .post("https://api.profile-stage.talenttechlab.org/auth/phone");

        String token = response2.jsonPath().get("token");
        String company = response2.jsonPath().get("company");

        given()
                .header("authorization", token)
                .header("x-company-id", company)
                .get("/v1/me")
                .then().log().body()
                .body("user.first_name", equalTo("Инга"));
    }

}
